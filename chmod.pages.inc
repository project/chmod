<?php
/**
 * @file
 * Page callbacks for chmod.
 */

/**
 * Admin settings form.
 */
function chmod_settings_form($form, &$form_state) {
  global $base_url;
  $cron_key = variable_get('cron_key', 'drupal');
  $replace = array(
    '%public' => $base_url . "/chmod/exec/public?cron_key=" . $cron_key,
  );
  $description = t('Especially the drush command "chmod-curl" is using this feature. The chmod command will only work when using cron key. Example: "%public"', $replace);

  $form['chmod_external'] = array(
    '#type' => 'checkbox',
    '#title' => t('Activate url "%url" for external access', array("%url" => 'chmod/exec/%')),
    '#description' => $description,
    '#default_value' => variable_get('chmod_external'),
  );
  return system_settings_form($form);
}

/**
 * Chmod execution form.
 */
function chmod_exec_form($form, &$form_state) {
  $paths = _chmod_paths();

  $system_user = exec("whoami");
  $replace = array(
    '%webuser' => $system_user,
  );
  $title = t('Path to exec (chmod -R g+w) as current system user: "%webuser"', $replace);
  $description = t("You can add more file paths in settings.php as described in README file.");

  $options = array();
  foreach ($paths as $key => $path) {
    $options[$key] = $key . ': ' . $path;
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Execute chmod command'),
  );

  $form['path_key'] = array(
    '#type' => 'select',
    '#title' => $title,
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => "public",
    '#description' => $description,
  );
  $form['#submit'][] = 'chmod_exec_submit';

  return $form;
}

/**
 * Submit function for chmod_change().
 */
function chmod_exec_submit($form, &$form_state) {
  $return = _chmod_exec($form_state['values']['path_key']);
  drupal_set_message($return['msg'], $return['op']);
}
