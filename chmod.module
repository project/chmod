<?php
/**
 * @file
 * Chmod main file.
 */

/**
 * Implements hook_menu().
 */
function chmod_menu() {
  $items = array();

  $items['admin/config/development/chmod'] = array(
    'title' => 'Chmod',
    'description' => 'File permissions helper module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('chmod_settings_form'),
    'file' => 'chmod.pages.inc',
    'access arguments' => array('use chmod'),
    'weight' => 10,
  );

  $items['admin/config/development/chmod/settings'] = array(
    'title' => 'Settings',
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/development/chmod/exec'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'chmod exec command (with webuser)',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('chmod_exec_form'),
    'access callback' => 'user_access',
    'access arguments' => array('use chmod'),
    'weight' => 10,
    'file' => 'chmod.pages.inc',
  );

  $items['chmod/exec/%'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Chmod exec via url',
    'page callback' => '_chmod_exec_url',
    'page arguments' => array(2),
    'access callback' => 'variable_get',
    'access arguments' => array('chmod_external', FALSE),
  );
  return $items;
}

/**
 * Implements hook_permission().
 */
function chmod_permission() {
  return array(
    'use chmod' => array(
      'title' => t('Use chmod'),
      'description' => t('Use chmod to change file permissions of files and folder.'),
    ),
  );
}

/**
 * Get paths standard and additional paths from settings.php.
 */
function _chmod_paths() {
  global $base_path;
  global $conf;
  $paths = array();

  $webroot = $_SERVER['DOCUMENT_ROOT'] . $base_path;
  $paths['root'] = $webroot;

  $tmp = variable_get('file_temporary_path', '');
  if ($tmp != '') {
    $paths['tmp'] = $tmp;
  }

  $public = variable_get('file_public_path', '');
  if ($public != '') {
    $paths['public'] = $public;
  }

  $private = variable_get('file_private_path', '');
  if ($private != '') {
    $paths['private'] = $private;
  }

  if (isset($conf['chmod']) && isset($conf['chmod']['paths'])) {
    foreach ($conf['chmod']['paths'] as $key => $path) {
      $paths[$key] = $path;
    }
  }

  return $paths;
}

/**
 * Callback function for "chmod/exec/%".
 */
function _chmod_exec_url($path_key = '') {
  $output = '';
  if (!isset($_GET['cron_key']) || variable_get('cron_key', 'drupal') != $_GET['cron_key']) {
    $output = "[error] Access denied";
  }
  else {
    $return = _chmod_exec($path_key);
    $output = "[" . $return['op'] . "] " . $return['msg'];
  }

  // Output the exec url page directly with no theming for speed.
  echo $output;

  // Exit early so we do not cache the data, nor do we wrap the result in a theme
  exit();
}

/**
 * Execute chmod command as webserver system user.
 */
function _chmod_exec($path_key = '') {
  $return = array();
  $return['op'] = 'error';
  $return['msg'] = '';
  $system_user = exec("whoami");
  $paths = _chmod_paths();

  if (array_key_exists($path_key, $paths)) {
    $path = $paths[$path_key];
  }
  else {
    $return['msg'] = t('Missing path key or empty argument.');
    return $return;
  }

  if (file_exists($path)) {
    $command = "find " . escapeshellarg($path) . " -user " . $system_user . " -exec chmod g+w {} +";
    exec($command);
    $replace = array(
      '%cmd' => $command,
      '%webuser' => $system_user,
    );
    $return['op'] = 'status';
    $return['msg'] = t('The following command was executed as system user "%webuser": "exec(%cmd)"', $replace);
  }
  else {
    $return['msg'] = t("Configured folder didn't exist.");
  }

return $return;
}
