The chmod.module is for changing file permissions. The first and most important
feature is to add write permissions on files where the webserver has the
ownership.

**This is primary for fixing a problem on recommended server configurations
where the webserver is running with a another system user than the system user
for ftp or ssh/drush.** On this systems there are sometimes files which cannot
be changed or deleted via drush or ftp command because of missing write
permissions for the file group. In most cases there are some kind of cache f
iles. If the drupal administrator has no access to the root user maybe on a
shared server (s)he can sometime use special command on the user-interface
of the hosting provider or need to open an issue there. 
This module helps to solve this situation directly with the webserver via the
browser. By design this can not be repaired via drush. Because of this a drush
command is not provided.

The current code is simply using find and chmod-command through "exec". This
very fast because we are using the power of the operating system. Maybe this
is not working on every PHP setup. But patches are welcome if somebody wants
to make this module more compatible with php file operations.

## Configuration

### Additional paths
If you want to add additional paths especially if there are some directories
outside of your webroot folder can this be configured in settings.php file.
The configuration array "$conf['chmod']['paths']" needs an additional key
naming like 'custom_key1' in following example:

    $conf['chmod']['paths']['custom_key1'] = '../customfolder';

The path keys including the predefined are shown on in the dropdown menu on
"admin/config/development/chmod". In future this keys will be used as
arguments on chmod drush commands.

### Drush usage
If you want to execute chmod via webserver system user you can need to 
'Activate url "chmod/exec/%" for external access' on the following page:
'admin/config/development/chmod'.

The "$base_url" has alos be to be set in settings.php. If there is a password
protection via .htaccess or in server configuration you can define username and
password in settings.php like this:

    $conf['curl-login'] = "user:password";

Yes, the password is saved in cleartext. But settings.php also contains the
password to your database and should be handled with care in any case.
