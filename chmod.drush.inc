<?php
/**
 * @file chmod.drush.inc
 */

/**
 * Implements hook_drush_command().
 */
function chmod_drush_command() {
  $items  = array();
  $items['chmod-curl'] = array(
    'description' => dt('Chmod command called via curl to operate as webserver system user.'),
    'arguments'   => array(
      'pathkey'   => "A path key default or defined by settings.php",
     ),
    'callback' => 'drush_chmod_curl',
    'bootstrap' => DRUSH_BOOTSTRAP_NONE,
  );
  return $items;
}

/**
 * Callback function for drush commad "chmod-curl".
 */
function drush_chmod_curl() {
  $external = variable_get('chmod_external', FALSE);
  if ($external == FALSE) {
    $message = t('The url "%url" for external access is not activated.', array("%url" => 'chmod/exec/%'));
    drush_set_error($message);
    return;
  }

  global $conf;
  global $base_url;
  $arguments = drush_get_arguments();
  $cron_key = variable_get('cron_key', 'drupal');

  // Set default path key to public files folder:
  $argument = 'public';
  if (isset($arguments[1])) {
    $argument = $arguments[1];
  }

  $login = '';
  if (isset($conf['curl-access'])) {
    $login = $conf['curl-access'];
  }

  $url = $base_url . "/chmod/exec/" . $argument . "?cron_key=" . $cron_key;

  $header = array();
  $header [] = "Accept: text/vnd.wap.wml,*.*";
  $agent = 'drush';
  $return = '';
  $ch = curl_init($url);
  if ($ch) {
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    if ($login != '') {
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
      curl_setopt($ch, CURLOPT_USERPWD, $login);
    }

    $return = curl_exec($ch);
  }

  $test = substr($return, 0, 8);
  if ($test == "[status]") {
    drupal_set_message($return, 'status');
  }
  else {
    if ($return == '') {
      drupal_set_message(t('The curl request was not successful.'), 'error');
    }
    else {
      drupal_set_message($return, 'error');
    }

  }

}

/**
 * Implements hook_drush_help().
 */
function chmod_drush_help($section) {
  switch ($section) {
    case 'drush:chmod-curl':
      return dt("Chmod command called via curl to operate as webserver system user.");
  }
}
